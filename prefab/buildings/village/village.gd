extends "../base/building.gd"

var _Production = 1.5

var _DeceptionS = 0.5

var satisfaction = 100

# GUI
onready var info_satisfaction_bar 	= info_panel.get_node("satisfaction_container/satisfaction_bar")

var accum = 0

# Ready
func _enter_tree():
	type = EBuildingType.Village
	
	needs = [{
		EBuildingType.Castle : 1,
		EBuildingType.Field : 5
		},
		{
		EBuildingType.Castle : 1,
		EBuildingType.Field : 10,
		EBuildingType.Barrack : 1
		},
		
		]

func _ready():
	# Satisfaction bar
	info_satisfaction_bar.max_value = 100
	info_satisfaction_bar.min_value = 0
	info_satisfaction_bar.value = 100
	
	

func on_post_state_action(p_delta):
	.on_post_state_action(p_delta)
	
	var satisfied = is_satisfied()
	if satisfied:
		g_globals.gold += _Production * p_delta * 1.3
	else:
		g_globals.gold += _Production * p_delta
			
	
	accum += p_delta
	if accum >= 1:
		if satisfied == false:
			satisfaction -= _DeceptionS
			
			# Exit condition - satisfaction = 0
			if satisfaction <= 0:
				g_scene_manager.go_to_scene_name("scn_lose")
			
		else:
			satisfaction += _DeceptionS * 4
		info_satisfaction_bar.value = satisfaction
		accum -= 1
			
			
func can_level_up():
	return info_satisfaction_bar.value == 100 and .can_level_up()