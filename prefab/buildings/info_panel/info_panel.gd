extends CanvasLayer

# Parent building
onready var building = get_parent()

# Building common info
onready var panel = g_node_utils.get_node(self, "panel")
onready var building_name_label = g_node_utils.get_node(panel, "container/center/name")

# Call it manually to update info
func update_info():
	building_name_label.text = building.name

func hide():
	panel.hide()

func show():
	panel.show()