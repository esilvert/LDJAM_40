extends "res://prefab/buildings/base/building.gd"

export var _Production = 0.4

# Ready
func _ready():
	type = EBuildingType.Field
	
	needs = [{
		EBuildingType.Village : 3
		},
		]

# Post state action
func on_post_state_action(p_delta):
	.on_post_state_action(p_delta)
	
	var satisfied = is_satisfied()
	if satisfied:
		g_globals.food += _Production * p_delta * 1.3
	else:
		g_globals.food += _Production * p_delta