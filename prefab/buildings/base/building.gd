extends Button

# Constants
const INFO_PANEL_POP_DURATION_S = 0.1

# Building states
enum EBuildingStates {
	Spawn,
	Working,
	Idle
	}

# Resource enumaration
enum EBuildingType {
	Castle
	Village,
	Field,
	Barrack,
	
	Count
	}
	
enum ECellType {
	Default,
	Buildable,
	Road
	}

export(String, "CASTLE", "VILLAGE", "FIELD", "BARRACK") var str_type = "CASTLE"# legacy standard
var type = EBuildingType.Castle
export var RADIUS = 1
export var SIZE = 1
export var EFFECTIVE_RADIUS = 5
export var LEVEL_UP_DURATION = 10
export var _MaxLevel = 0

## Common nodes
# Info panel
onready var info_panel 				= get_node("gui/info_panel")
onready var info_name 				= info_panel.get_node("center/name")
onready var info_level 				= info_panel.get_node("level")
onready var lvl_up_bar 				= get_node("sprite")
onready var info_needs = {
	EBuildingType.Castle 		: info_panel.get_node("CASTLE_nb"),
	EBuildingType.Village 		: info_panel.get_node("CITY_nb"),
	EBuildingType.Field 		: info_panel.get_node("FIELD_nb"),
	EBuildingType.Barrack 		: info_panel.get_node("BARRACK_nb"),
	}

var try_level_up = false
var level_up = false

# Hover management
var is_hovered = false
var hovering_duration = 0

var level = 0
# Needs
var needs setget set_needs, get_current_needs
func set_needs():
	assert(false)
func get_current_needs():
	if needs == null:
		needs = [{}]
	return needs[level]


# Provided
var provided = {}

var connections = []

func _enter_tree():
	for i in range(0, EBuildingType.Count):
			get_current_needs()[i] = 0
			provided[i] = 0

func _ready():
	# Connections
	connect("mouse_entered",	self, 	"_on_mouse_entered")
	connect("mouse_exited", 	self, 	"_on_mouse_exited")
	
	var navigation = get_tree().get_nodes_in_group("navigation")[0]
	
	var buildings = get_tree().get_nodes_in_group("buildings")
	for building in buildings:
		var delta = (building.get_global_rect().position - get_global_rect().position )/64
		var in_my_radius = abs(delta.x) < EFFECTIVE_RADIUS + SIZE/2 and abs(delta.y) < EFFECTIVE_RADIUS + SIZE/2
		var in_its_radius = abs(delta.x) < building.EFFECTIVE_RADIUS + building.SIZE/2 and abs(delta.y) < building.EFFECTIVE_RADIUS + building.SIZE/2
		prints("Building name ", building.get_name(), "ER:", building.EFFECTIVE_RADIUS, "SIZE:", building.SIZE)
		var exists_path = navigation.get_simple_path(get_global_rect().position, building.get_global_rect().position).size() > 0
		if (in_its_radius or in_my_radius) and building != self and exists_path:
			inter_connect_with(building)
		
	add_to_group("buildings")

# Post state action
func on_post_state_action(p_delta):
	# hover management
	if is_hovered == true:
		hovering_duration += p_delta
		
		# Info popup
		if hovering_duration > INFO_PANEL_POP_DURATION_S:
			on_gui_updated()
			info_panel.show()
	else:
		info_panel.hide()
		
	# Level up
	if try_level_up == true:
		if can_level_up():
			level_up = true
			try_level_up = false
#			prints(get_name(), "ready to lvl up.")
			

# Callback : mouse entered
func _on_mouse_entered():
	hovering_duration = 0
	is_hovered = true

# Callback : mouse exited
func _on_mouse_exited():
	is_hovered = false

func can_level_up():
	return is_satisfied() and not is_max_level()

# GUI Updated
func on_gui_updated():
	info_name.text = g_globals.building_type_to_str[str_type]
	info_level.text = "Level : " + str(level+1)
	update_provided()
	
	var string = ""
	
	for i in range(0, EBuildingType.Count):
		var need = 0
		if get_current_needs().has(i) : need = get_current_needs()[i]
		
		var provi = 0
		if provided.has(i): provi = provided[i]
		
		info_needs[i].text = str(provi) + '/' + str(need)

# Interconnection with another building
func inter_connect_with(p_building):
	if connections.find(p_building) == -1:
		p_building.add_connection(self)
		add_connection(p_building)

func add_connection(p_building):
#	prints(get_name(), "added connection with ", p_building.get_name())
	connections.append(p_building)
	update_provided()
	
	
# Updated which building is near
func update_provided():
	for i in range(0, EBuildingType.Count):
		provided[i] = 0
	
	for connection in connections:
		provided[connection.type] += 1
	
	if is_satisfied() == true:
		if is_max_level() == false:
#		s	prints(get_name(), "try to lvl up.")
			try_level_up = true and level_up == false
#		else:
#			prints("Level = " , level, " needs = " , get_current_needs(), "provided =", provided, "max_level=" , needs.size())
#	else:	
#		prints(get_name(), "Cannot level up : satisfied = " , is_satisfied() , " | max_level =", is_max_level(), get_current_needs(), provided)
	
		
func is_satisfied():
	var current_needs = get_current_needs()
	for i in current_needs.keys():
		if current_needs[i] > provided[i]:
			return false
	return true
	
func is_max_level():
	return level == _MaxLevel