extends "../base/building.gd"


# GUI
onready var info_satisfaction_bar 	= info_panel.get_node("satisfaction_container/satisfaction_bar")


# Satisfaction
# If fall to 0 the game is lost
var satisfaction = 100
export var _GoldProductionS = 1

var _DeceptionS = 0.16
var accum = 0

func _enter_tree():
	# Needs
	needs = [{
		EBuildingType.Field : 1
	},
	{
		EBuildingType.Field 	: 5,
		EBuildingType.Village 	: 1
	},
	{
		EBuildingType.Field 	: 10,
		EBuildingType.Village 	: 5,
	},
	{
		EBuildingType.Field 	: 15,
		EBuildingType.Village 	: 8,
		EBuildingType.Barrack 	: 1,
	},
	{
		EBuildingType.Field 	: 18,
		EBuildingType.Village 	: 10,
		EBuildingType.Barrack 	: 3,
	},
	]

func _ready():
	## Init GUI
	# Satisfaction bar
	info_satisfaction_bar.max_value = 100
	info_satisfaction_bar.min_value = 0
	info_satisfaction_bar.value = 100
	
	on_gui_updated()

# Post state action
func on_post_state_action(p_delta):
	# Base
	.on_post_state_action(p_delta)
	
	## GUI
	# Satisfaction bar
	info_satisfaction_bar.value = satisfaction
	accum += p_delta
	
	if accum >= 1 :
		if is_satisfied() == false:
			satisfaction -= _DeceptionS
			g_globals.gold += _GoldProductionS
		else:
			satisfaction += _DeceptionS * 4
			g_globals.gold += _GoldProductionS * 1.3
		accum -= 1
		info_satisfaction_bar.value = satisfaction
	
	if level == _MaxLevel and info_satisfaction_bar.value >= 100:
		get_tree().call_group("root", "_on_succeeded")
	
	# Exit condition - satisfaction = 0
	if satisfaction == 0:
		g_sound_manager.play("scn_changed")
		g_scene_manager.go_to_scene_name("scn_lose")
	