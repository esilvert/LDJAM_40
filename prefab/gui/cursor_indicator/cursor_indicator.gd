extends Sprite

var building_src = preload("res://prefab/buildings/base/building.gd")

var default = preload("res://prefab/gui/cursor_indicator/cursor_indicator.png")
var castle = preload("res://prefab/buildings/castle/castle.png")
var village = preload("res://prefab/buildings/village/village.png")
var field = preload("res://prefab/buildings/field/field.png")
var barrack = preload("res://prefab/buildings/barrack/barrack.png")

var type setget set_type
func set_type(p_value):
	type = p_value
	if type == building_src.EBuildingType.Castle:
		texture = castle
	elif type == building_src.EBuildingType.Village:
		texture = village
	elif type == building_src.EBuildingType.Field:
		texture = field
	elif type == building_src.EBuildingType.Barrack:
		texture = barrack
	else:
		texture = default
	