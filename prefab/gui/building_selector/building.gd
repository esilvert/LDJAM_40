extends TextureButton

export var _Cost = 500
export var _Food = 2000

onready var cost = get_node("cost")
onready var food = get_node("food")

func _ready():
	cost.text = str(_Cost)
	food.text = str(_Food)
	
	set_process(true)
	
func _process(p_delta):
	disabled = g_globals.gold < _Cost or g_globals.food < _Food
	
func _pressed():
	g_sound_manager.play("button_pressed")