extends Panel

var buildin_src = preload("res://prefab/buildings/base/building.gd")

var current_type = null
var current_cost = {"gold":0, "food":0}

# Ready
func _ready():
	set_process(true)

# Process
func _process(p_delta):
	var camera = get_tree().get_nodes_in_group("camera")[0]
	var camera_center = camera.get_camera_center()
	
	var mouse_pos = get_global_mouse_position()
	mouse_pos -= Vector2(int(mouse_pos.x) % 64, int(mouse_pos.y) % 64)
	mouse_pos -= camera.get_viewport_rect().size * 0.5
	camera.cursor_type = current_type
	
	if Input.is_action_pressed("ui_cancel") or Input.is_mouse_button_pressed(BUTTON_RIGHT):
		g_sound_manager.play("cancel")
		current_type = null
		current_cost = {"gold":0, "food":0}
		camera.cursor_type = null
	
	if  Input.is_mouse_button_pressed(BUTTON_LEFT) == true and current_type != null and get_rect().has_point(get_local_mouse_position()) == false:
		var building = get_tree().get_nodes_in_group("map")[0].add_building(camera_center, current_type)
		if building != null:
			g_globals.gold -= current_cost["gold"]
			g_globals.food -= current_cost["food"]
			current_type = null
			current_cost = {"gold":0, "food":0}
			camera.cursor_type = null
			g_sound_manager.play("success")
		else:
			g_sound_manager.play("error")
			
			
	
# Callback - Castle pressed
func _on_castle_pressed():
	current_type = buildin_src.EBuildingType.Castle
	current_cost = {"gold":get_node("castle")._Cost, "food":get_node("castle")._Food}
	pass # replace with function body

# Callback - Village pressed
func _on_village_pressed():
	current_type = buildin_src.EBuildingType.Village
	current_cost = {"gold":get_node("village")._Cost, "food":get_node("village")._Food}	
	pass # replace with function body


func _on_field_pressed():
	current_type = buildin_src.EBuildingType.Field
	current_cost = {"gold":get_node("field")._Cost, "food":get_node("field")._Food}	
	pass # replace with function body


func _on_barrack_pressed():
	current_type = buildin_src.EBuildingType.Barrack
	current_cost = {"gold":get_node("barrack")._Cost, "food":get_node("barrack")._Food}	
	pass # replace with function body
