extends KinematicBody2D


const SPEED = 64*1500

func _ready():
	set_physics_process(true)
	
func _physics_process(delta):
	var direction = Vector2()
	if Input.is_action_just_pressed("ui_right"):
		move_and_slide(Vector2(64,0))
	if Input.is_action_just_pressed("ui_left"):
		move_and_slide(Vector2(-64,0))
	if Input.is_action_just_pressed("ui_up"):
		move_and_slide(Vector2(0,-64))
	if Input.is_action_just_pressed("ui_down"):
		move_and_slide(Vector2(0,64))
		