extends Navigation2D

# Orders
var orders = []

#FSM
onready var fsm = get_node("control_fsm")

# Map
onready var map = get_node("map")

# updated on click and clicked appended to orders
var updated = false

# ready
func _ready():
	fsm.connect("stateChanged", self, "_on_state_changed")
	
	# Temp
#
#	castle.connect("pressed", self, "_on_child_pressed", [castle])
#	village.connect("pressed", self, "_on_child_pressed", [village])
	
	# temp

# callback - child pressed
func _on_child_pressed(p_child):
	orders.append(p_child)
	updated = true
	
# Callback = state changed
func _on_state_changed(p_new, p_old):
	updated = false
