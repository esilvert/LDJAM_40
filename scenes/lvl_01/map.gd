extends TileMap


var buildin_src = preload("res://prefab/buildings/base/building.gd")

# Scene - Buildings
var castle_scn = preload("res://prefab/buildings/castle/castle.tscn")
var village_scn = preload("res://prefab/buildings/village/village.tscn")
var field_scn = preload("res://prefab/buildings/field/field.tscn")
var barrack_scn = preload("res://prefab/buildings/barrack/barrack.tscn")

export var CIVILIZED_CELL = 1
export var ROAD_CELL = 2

var occupied_cells = []


func add_building(p_pos_v, p_type):
	var v_pos = world_to_map(p_pos_v)
	
	
	var building = null
	if p_type == buildin_src.EBuildingType.Castle:
		building = castle_scn.instance()
	elif p_type == buildin_src.EBuildingType.Village:
		building = village_scn.instance()
	elif p_type == buildin_src.EBuildingType.Field:
		building = field_scn.instance()
	elif p_type == buildin_src.EBuildingType.Barrack:
		building = barrack_scn.instance()
	else:
		assert(false)
		
	# Check available
	if is_area_free( v_pos, building.SIZE, g_globals.building_origin_tile[building.str_type]) == false:
		building.free()
		return null
	
	building.set_global_position( p_pos_v )
	add_child(building)
		
	# Update map
	add_occupied_cell(v_pos, building.SIZE)
	
	var tile_idx = CIVILIZED_CELL
	if p_type == buildin_src.EBuildingType.Village:
		tile_idx = ROAD_CELL
	add_area(v_pos, building.RADIUS, building.SIZE, tile_idx)
	return building

# Add occupied cells
func add_occupied_cell(p_pos_v, p_radius):
	for w in range(0, p_radius):
		for h in range(0, p_radius):
			occupied_cells.append(p_pos_v + Vector2(w,h))
	

# Return if area is available
func is_area_free(p_pos_v, p_radius, p_cell_required):
	for w in range(0, p_radius):
		for h in range(0, p_radius):
			var idx = occupied_cells.find(p_pos_v + Vector2(w,h)) # CHECK IF WORKING
			var cell_type = get_cellv(p_pos_v + Vector2(w,h))
			if idx != -1 or cell_type != p_cell_required:
				return false
	return true

# Set area as civil
func add_area( p_pos_v, p_radius, p_size, p_cell_value):
	for x in range(p_pos_v.x - p_radius - 1, p_pos_v.x + p_size + p_radius ):
		for y in range(p_pos_v.y - p_radius - 1, p_pos_v.y + p_size + p_radius ):
			var offset = Vector2()
			if x > p_pos_v.x: offset.x = p_size - (p_pos_v.x - x) - 1
			if y > p_pos_v.y: offset.y = p_size - (p_pos_v.y - y) - 1
			var dx = abs(p_pos_v.x + offset.x - x)
			var dy = abs(p_pos_v.y + offset.y - y)
			if get_cellv(Vector2(x,y)) < p_cell_value and dx  <= p_radius and dy <= p_radius:
				set_cellv( Vector2(x,y), p_cell_value)
	