extends Node2D

export(String) var next_scn = "lvl_02"
export(int) var base_gold = 5300
export(int) var base_food = 2300

func _ready():
	add_to_group("root")
	g_globals.food = base_food
	g_globals.gold = base_gold

func _on_succeeded():
	g_sound_manager.play("scn_changed")
	g_scene_manager.go_to_scene_name(next_scn)