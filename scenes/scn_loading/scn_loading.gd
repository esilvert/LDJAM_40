extends CanvasLayer

# Total duration of process
var process_duration = 0 

# Scene is display after duration is over this value
const MIN_DELAY_DURATION = 1 # seconds

# The progress bar node
onready var progress_bar = get_node("progress_bar")

# Ready
func _ready():
	process_duration = 0
	set_process(true)

# Process
func _process(delta):
	process_duration += delta
	
	if process_duration >= MIN_DELAY_DURATION:
		var children = get_children()
		for child in children :
			child.visible = true


