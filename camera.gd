extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var sprite = get_node("sprite")

var cursor_type setget set_cursor_type
func set_cursor_type(p_value):
	sprite.type = p_value

func get_camera_center():
	return sprite.global_position

func _ready():
	set_process(true)
	
func _process(delta):
	if Input.is_action_just_pressed("ui_right"):
		position += Vector2(64,0)
	if Input.is_action_just_pressed("ui_left"):
		position += Vector2(-64,0)
	if Input.is_action_just_pressed("ui_up"):
		position += Vector2(0,-64)
	if Input.is_action_just_pressed("ui_down"):
		position += Vector2(0,64)
		
	var mouse_pos = get_local_mouse_position()
	
	if mouse_pos.x > 0 :
		mouse_pos.x -= int(mouse_pos.x) % 64
	else:
		mouse_pos.x += int(abs(mouse_pos.x)) % 64 -64
	
	if mouse_pos.y > 0 :
		mouse_pos.y -= int(mouse_pos.y) % 64
	else:
		mouse_pos.y -= int(mouse_pos.y) % 64 + 64
			
	
	sprite.position = mouse_pos 