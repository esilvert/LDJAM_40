extends Node
#
# Scene manager (g_scene_manager)
# def: Allow to work on scenes
#
const SCENE_FOLDER_PATH  = "res://scenes/"

# Signal when scene has changed
signal scene_changed

# Current scene played, synchronized with get_tree().current_scene
var current_scene = null
var _next_scene = null # intern use

# The color used during fading
var fading_color = Color(0,0,0)
	
# Switch to packed scene
func go_to_scene_packed( p_packed_scene):
	call_deferred("_go_to_scene_packed", p_packed_scene)
	g_render_manager.fade_to(fading_color)
	
# Effective go_to scene packed
func _go_to_scene_packed( p_packed_scene ):
	_switch_scene_fading( p_packed_scene.instance() )

# Go to a loaded scene
func go_to_scene( p_new_scene):
	call_deferred("_switch_scene_fading", p_new_scene)
	g_render_manager.fade_to(fading_color)

# Go to a scene given by its name
func go_to_scene_name( p_name) :
	g_loading_manager.connect("loading_finished", self, "_on_resource_loaded")
	g_loading_manager.load_interactive(SCENE_FOLDER_PATH + p_name + "/" + p_name + ".tscn")
	g_render_manager.fade_to(fading_color)

# Callback while loading a scene
func _on_resource_loaded(p_new_scene):
	_switch_scene_fading(p_new_scene.instance())
	g_loading_manager.disconnect("loading_finished", self, "_on_resource_loaded")

# Switch with fading
func _switch_scene_fading(p_new_scene):
	_next_scene = p_new_scene
	set_process(true)

# Configure new scene
func _switch_scene( p_new_scene ):
	if current_scene != null:
		current_scene.queue_free()
	else:
		get_tree().get_current_scene().queue_free()
	
	current_scene = p_new_scene
	get_node("/root").add_child(p_new_scene)
	get_tree().set_current_scene(p_new_scene)
	emit_signal("scene_changed", p_new_scene)
	print( "g_scene_manager._switch_scene(): Switched scene.")

# Process
func _process(delta):
	if _next_scene != null and g_render_manager.is_fading == false:
		_switch_scene(_next_scene)
		g_render_manager.fade_to( Color(1,1,1) )
		_next_scene = null