extends Node

#
# Sound Manager (g_sound_manager)
# def: Allows to play a sound
#
const SOUND_FOLDER_PATH = "res://assets/sounds/"

var sample_library = null

var sounds = {}

# Ready
func _ready():
#	sample_library = SampleLibrary.new()
#	sample_player.set_sample_library( sample_library )
	pass

# Play (and possibly load) a sound
func play(p_id, p_volume = -25, p_loop = false):
	if not sounds.has(p_id) :
		var sample = ResourceLoader.load(SOUND_FOLDER_PATH + p_id + ".wav")
		var streamer = AudioStreamPlayer.new()
		streamer.stream = sample
		sounds[p_id] = streamer
		add_child(streamer)
		
	sounds[p_id].volume_db = p_volume
	sounds[p_id].play()