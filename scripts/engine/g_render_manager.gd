extends Node

#
# Render Manager (g_render_manager)
# def: Allows to manipulate rendering
#

# Fading Duration 
const SWITCHING_SCENE_FADING_DURATION = 0.5

# Global modulate
onready var modulate = CanvasModulate.new()

# Target color for modulate
var target_color = Color(1,1,1)

# Current process duration
var process_duration = 0

# Target duration for modulate
var target_duration = -1

# True while fading
var is_fading = false

# Ready
func _ready():
	add_child(modulate)
	set_process(true)

# Allows to fade
func fade_to(p_color, p_duration = SWITCHING_SCENE_FADING_DURATION ):
	assert( p_duration != 0)
	target_duration = p_duration
	target_color = p_color
	process_duration = 0
	is_fading = true

# Process
func _process(delta):
	process_duration += delta
	
	# Fading
	if is_fading == true:
		modulate.set_color( modulate.get_color().linear_interpolate(target_color, process_duration / target_duration) )
		if process_duration > target_duration:
			modulate.set_color(target_color)
			is_fading = false