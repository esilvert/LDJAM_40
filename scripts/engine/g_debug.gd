extends Node

#
# Debug (g_debug)
# def: Allow to debug
#

# Start time 
onready var start_time = OS.get_ticks_msec()

# Print a message
func print_message(p_message):
	print( str(int(start_time)), ",\t",  p_message )

# Print a message on assert
func assert_message(p_condition, p_message):
	if p_condition == false:
		print_message("Assertion failed : " + p_message)
		assert(p_condition)
		