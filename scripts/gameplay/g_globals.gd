#
# g_globals
#
extends Node2D

var building_src = preload("res://prefab/buildings/base/building.gd")

var food = 2300
var gold = 6000

func _ready():
	g_sound_manager.play("bg", -15, false);
	
var building_type_to_str = {
		"CASTLE" 	: 	"HQ",
		"VILLAGE" 	: 	"City",
		"FIELD"		:	"Food Center",
		"BARRACK"		:	"Barrack",
	}
	
var building_origin_tile = {
		"CASTLE" 		: 	building_src.ECellType.Road,
		"VILLAGE" 		: 	building_src.ECellType.Buildable,
		"FIELD"			:	building_src.ECellType.Buildable,
		"BARRACK"		:	building_src.ECellType.Road,
	}